using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace AdventOfCode2020
{
    internal class Day7 : DayBase
    {
        internal override void Solve()
        {            
            IEnumerable<string> input = Util.ReadPuzzleInput("Day7_1.txt");
            var bags = new Dictionary<string, List<Bag>>();
            input.ToList().ForEach(line => {
                string mainBagColor = Regex.Match(line, @"^[a-z]+\s[a-z]+").Value;
                var v = line.Split("contain")[1]
                            .Split(",")
                            .Select(s => {
                                s.Trim();
                                return s.Contains("no other bags") 
                                    ? new Bag() 
                                    : new Bag()
                                        {
                                            Color = Regex.Match(s, @"[a-z]+\s[a-z]+").Value,
                                            Count = int.Parse(Regex.Match(s, @"\d{1}").Value)
                                        };
                            })
                            .ToList();
                bags.Add(mainBagColor, v);
            });

            string colorToFind = "shiny gold";            
            
            PrintAnswer(7, 1, bags.Select(kvp => kvp.Key).Where(bag => FindColor(bag, colorToFind, bags)).Count());
            PrintAnswer(7, 2, FindAllBags(colorToFind, bags) - 1);
        }

        private bool FindColor(string seed, string colorToFind, Dictionary<string, List<Bag>> bags)
        {
            if (seed == null || bags[seed] == null)
            {
                return false;
            }
        
            foreach(var b in bags[seed])
            {
                if (b.Color == colorToFind || FindColor(b.Color, colorToFind, bags)) 
                {
                    return true;
                }                
            }

            return false;
        }
        
        private int FindAllBags(string seed, Dictionary<string, List<Bag>> bags)
        {
            if (seed == null || bags[seed] == null) 
            {
                return 0;
            }
            
            return bags[seed].Select(b => b.Count * FindAllBags(b.Color, bags)).Sum() + 1;
        }
    }

    internal class Bag
    {
        internal string Color { get; set; }
        internal int Count { get; set; }
    }
}