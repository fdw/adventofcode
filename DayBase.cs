namespace AdventOfCode2020
{
    internal abstract class DayBase
    {
        internal abstract void Solve();

        internal void PrintAnswer(int day, int part, int answer)
        {
            PrintAnswer(day, part, answer.ToString());
        }

        internal void PrintAnswer(int day, int part, long answer)
        {
            PrintAnswer(day, part, answer.ToString());
        }

        private void PrintAnswer(int day, int part, string answer)
        {
            System.Console.WriteLine($"Solution for day {day}, part {part}: {answer}");
        }
    }
}