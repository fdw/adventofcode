using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace AdventOfCode2020
{
    internal class Day8 : DayBase
    {
        internal override void Solve()
        {
            string[] input = Util.ReadPuzzleInput("Day8_1.txt").ToArray();
            RunProgram(input, out int acc);
            PrintAnswer(8, 1, acc);
            ReplaceOps(input, "jmp", "nop");
            ReplaceOps(input, "nop", "jmp");            
        }

        private void ReplaceOps(string[] input, string op1, string op2)
        {
            for(int i = 0; i < input.Length; i++)
            {
                if (input[i].Substring(0, 3).Equals(op1))
                {
                    var temp = new string[input.Length];
                    System.Array.Copy(input, temp, input.Length);
                    temp[i] = temp[i].Replace(op1, op2);
                    if (RunProgram(temp, out int accumulator))
                    {
                        PrintAnswer(8, 2, accumulator);
                        break;
                    }
                }
            }       
        }

        private bool RunProgram(string[] input, out int accumulator)
        {
            var visitedIndexes = new HashSet<int>();            
            int a = 0;
            for(int i = 0; i < input.Length; i++)
            {
                if (!visitedIndexes.Add(i))
                {
                    accumulator = a;
                    return false;
                }

                string op = input[i].Substring(0, 3);
                int amount = int.Parse(Regex.Match(input[i], @"(\+|-)\d+").Value);

                if (op.Equals("nop"))
                {
                    continue;
                }
                else if (op.Equals("acc"))
                {
                    a += amount;
                }
                else if (op.Equals("jmp"))
                {
                    i += amount - 1;
                }    
            }

            accumulator = a;
            return true;
        }
    }
}