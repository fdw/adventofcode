using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace AdventOfCode2020
{
    internal class Day5 : DayBase
    {
        internal override void Solve()
        {
            IEnumerable<string> input = Util.ReadPuzzleInput("Day5_1.txt");
            
            var seatIds = input
                .Select(s => Regex.Replace(Regex.Replace(s, "B|R", "1"), "F|L", "0"))
                .Select(s => 
                    (
                        row: Convert.ToInt32(s.Substring(0, 7), 2),
                        column: Convert.ToInt32(s.Substring(7, 3), 2)
                    ))
                .Select(p => p.row * 8 + p.column)
                .ToList();

            seatIds.Sort();

            PrintAnswer(5, 1, seatIds[seatIds.Count - 1]);

            int mySeat = seatIds
                .Skip(1)
                .Zip(seatIds, (curr, prev) => curr - prev == 2 ? curr - 1 : -1)
                .First(i => i != -1);

            PrintAnswer(5, 2, mySeat);
        }
    }
}