using System.Collections.Generic;

namespace AdventOfCode2020
{
    internal class Day2 : DayBase
    {
        internal override void Solve()
        {
            IEnumerable<string> input = Util.ReadPuzzleInput("Day2_1.txt");

            int correctCount1 = 0;
            int correctCount2 = 0;

            foreach (string s in input)
            {
                string[] parts = s.Split(" ");
                string[] bounds = parts[0].Split("-");
                int lowerBound = int.Parse(bounds[0]);
                int higherBound = int.Parse(bounds[1]);
                char letter = parts[1][0];
                string password = parts[2];

                int letterCount = 0;

                foreach (char c in password)
                {
                    if (c == letter)
                    {
                        letterCount++;
                    }
                }

                if (letterCount >= lowerBound && letterCount <= higherBound)
                {
                    correctCount1++;
                }

                if ((password[lowerBound - 1] == letter || password[higherBound - 1] == letter) 
                    && password[lowerBound - 1] != password[higherBound - 1])
                {
                    correctCount2++;
                }
            }

            PrintAnswer(2, 1, correctCount1);
            PrintAnswer(2, 2, correctCount2);
        }
    }
}