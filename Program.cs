﻿using System;
using System.Reflection;
using System.Text.RegularExpressions;

namespace AdventOfCode2020
{
    class Program
    {
        static void Main(string[] args)
        {
            string classNameRegex = @"Day\d{1,2}$";
            string methodName = "Solve";
            Array.ForEach(Assembly.GetExecutingAssembly().GetTypes(), t => {
                if (t.IsClass && Regex.Match(t.Name, classNameRegex, RegexOptions.IgnoreCase).Success)
                {
                    var instance = Activator.CreateInstance(t);
                    var method = t.GetMethod(methodName, BindingFlags.Instance | BindingFlags.NonPublic);
                    method.Invoke(instance, null);
                }
            });
        }
    }
}
