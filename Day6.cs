using System.Linq;
using System.Collections.Generic;

namespace AdventOfCode2020
{
    internal class Day6 : DayBase
    {
        internal override void Solve ()
        {            
            string input = Util.ReadPuzzleInputAsString("Day6_1.txt");            
            var groups = input.Split("\r\n\r\n");
            int anyone = groups
                .Select(group => group.Replace("\r\n", string.Empty).Distinct().Count())
                .Sum(yesAnswers => yesAnswers);

            int everyone = groups
                .Select(group => {
                    return 
                        group.Split("\n", System.StringSplitOptions.RemoveEmptyEntries)
                            .Aggregate<IEnumerable<char>>((accumulated, next) => accumulated.Intersect(next));
                })
                .Sum(yesAnswers => yesAnswers.Count());
            
            PrintAnswer(6, 1, anyone);
            PrintAnswer(6, 2, everyone);
        }
    }
}