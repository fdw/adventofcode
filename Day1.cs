using System.Linq;
using System.Collections.Generic;

namespace AdventOfCode2020
{
    internal class Day1 : DayBase
    {
        internal override void Solve()
        {
            IEnumerable<string> inputS = Util.ReadPuzzleInput("Day1_1.txt");
            List<int> input = inputS.Select(i => int.Parse(i)).ToList();

            int entry1 = -1;
            int entry2 = -1;

            foreach(int nr1 in input)
            {
                foreach(int nr2 in input)
                {
                    if (nr1 + nr2 == 2020)
                    {
                        entry1 = nr1;
                        entry2 = nr2;
                        break;
                    }
                }
            }

            PrintAnswer(1, 1, entry1 * entry2);

            int entry3 = -1;

            foreach (int nr1 in input)
            {
                foreach (int nr2 in input)
                {
                    foreach (int nr3 in input)
                    {
                        if (nr1 + nr2 + nr3 == 2020)
                        {
                            entry1 = nr1;
                            entry2 = nr2;
                            entry3 = nr3;
                            break;
                        }
                    }
                }
            }

            PrintAnswer(1, 2, entry1 * entry2 * entry3);
        }
    }
}