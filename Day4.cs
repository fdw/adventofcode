using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace AdventOfCode2020
{
    internal class Day4 : DayBase
    {
        internal override void Solve()
        {
            string[] input = Util.ReadPuzzleInput("Day4_1.txt").ToArray();
            var simplePassports = new List<Passport>();
            var complexPassports = new List<ComplexPassport>();

            var currentSimplePassport = new Passport();
            var currentComplexPassport = new ComplexPassport();
            for(int i = 0; i < input.Length; i++)
            {
                if (string.IsNullOrEmpty(input[i]))
                {
                    simplePassports.Add(currentSimplePassport);
                    complexPassports.Add(currentComplexPassport);
                    currentSimplePassport = new Passport();
                    currentComplexPassport = new ComplexPassport();
                    continue;
                }

                var categories = input[i].Split(" ");
                ParseCategories(currentSimplePassport, currentComplexPassport, categories);

                if (i == input.Length - 1)
                {
                    simplePassports.Add(currentSimplePassport);
                    complexPassports.Add(currentComplexPassport);
                }
            }

            PrintAnswer(4, 1, simplePassports.Count(p => p.IsValid()));
            PrintAnswer(4, 2, complexPassports.Count(p => p.IsValid()));
        }

        private void ParseCategories(Passport simplePassport, ComplexPassport complexPassport, string[] categories)
        {
            foreach (string category in categories)
            {
                string[] kvp = category.Split(":");
                string categoryName = kvp[0];
                string categoryValue = kvp[1];

                switch (categoryName)
                {
                    case "byr":
                        simplePassport.Byr = complexPassport.Byr = int.TryParse(categoryValue, out int byr) ? byr : -1;                         
                        break;
                    case "iyr":
                        simplePassport.Iyr = complexPassport.Iyr = int.TryParse(categoryValue, out int iyr) ? iyr : -1;
                        break;
                    case "eyr":
                        simplePassport.Eyr = complexPassport.Eyr = int.TryParse(categoryValue, out int eyr) ? eyr : -1;
                        break;
                    case "hgt":
                        simplePassport.Hgt = complexPassport.Hgt = categoryValue;
                        break;
                    case "hcl":
                        simplePassport.Hcl = complexPassport.Hcl = categoryValue;
                        break;
                    case "ecl":
                        simplePassport.Ecl = complexPassport.Ecl = categoryValue;
                        break;
                    case "pid":
                        simplePassport.Pid = complexPassport.Pid = categoryValue;
                        break;
                    case "cid":
                        simplePassport.Cid = complexPassport.Cid = categoryValue;
                        break;
                    default: 
                        return;
                }
            }
        }
    }

    internal class Passport
    {
        internal virtual int Byr { get; set; } = -1;
        internal virtual int Iyr { get; set; } = -1;
        internal virtual int Eyr { get; set; } = -1;
        internal virtual string Hgt { get; set; }
        internal virtual string Hcl { get; set; }
        internal virtual string Ecl { get; set; }
        internal virtual string Pid { get; set; }
        internal virtual string Cid { get; set; }

        internal bool IsValid()
        {
            return Byr != -1
                && Iyr != -1
                && Eyr != -1
                && Hgt != null
                && Hcl != null
                && Ecl != null
                && Pid != null;
        }
    }

    internal class ComplexPassport : Passport
    {
        private int _byr;
        internal override int Byr
        {
            get => _byr;
            set => _byr = value >= 1920 && value <= 2002 ? value : -1; 
        }

        private int _iyr;
        internal override int Iyr
        {
            get => _iyr;
            set => _iyr = value >= 2010 && value <= 2020 ? value : -1;
        }

        private int _eyr;
        internal override int Eyr
        {
            get => _eyr;
            set => _eyr = value >= 2020 && value <= 2030 ? value : -1;
        }

        private string _hgt;
        internal override string Hgt
        {
            get => _hgt;
            set {
                int.TryParse(value.Substring(0, value.Length - 2), out int length);
                string measurement = value.Substring(value.Length - 2, 2);

                if (measurement == "cm")
                {
                    _hgt = length >= 150 && length <= 193 ? value : null;
                }
                else if (measurement == "in")
                {
                    _hgt = length >= 59 && length <= 76 ? value : null;
                }
                else
                {
                    _hgt = null;
                }
            }
        }

        private string _hcl;
        internal override string Hcl
        {
            get => _hcl;
            set => _hcl = Regex.Match(value, @"#[0-9a-z]{6}").Success ? value : null; 
        }

        private string _ecl;
        internal override string Ecl
        {
            get => _ecl;
            set => _ecl = Regex.Match(value, @"^amb$|^blu$|^brn$|^gry$|^grn$|^hzl$|^oth$").Success ? value : null;
        }

        private string _pid;
        internal override string Pid
        {
            get => _pid;
            set => _pid = Regex.Match(value, @"^\d{9}$").Success ? value : null;
        }
    }
}
