using System.IO;
using System.Collections.Generic;

namespace AdventOfCode2020
{
    internal class Util
    {
        internal static IEnumerable<string> ReadPuzzleInput(string fileName)
        {
            return File.ReadAllLines(Path.Combine(Configuration.PuzzleInputDir, fileName));
        }

        internal static string ReadPuzzleInputAsString (string fileName)
        {
            return File.ReadAllText(Path.Combine(Configuration.PuzzleInputDir, fileName));
        }
    }
}