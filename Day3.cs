using System.Linq;
using System.Collections.Generic;

namespace AdventOfCode2020
{
    internal class Day3 : DayBase
    {
        internal override void Solve()
        {
            string[] input = Util.ReadPuzzleInput("Day3_1.txt").ToArray();

            var slopes = new List<(int x, int y)>
            {
                (x: 1, y: 1),
                (x: 3, y: 1),
                (x: 5, y: 1),
                (x: 7, y: 1),
                (x: 1, y: 2)
            };

            long totalTreeCount = 1;

            foreach (var slope in slopes)
            {
                int x = 0;
                int mapWidth = input[0].Length;
                int treeCountForSlope = 0;
                for(int y = slope.y; y < input.Length; y = y + slope.y)
                {                
                    x = x + slope.x;
                    int xCoord = x % mapWidth;
                    if(input[y][xCoord] == '#')
                    {
                        treeCountForSlope++;
                    }
                }
                
                if (slope.x == 3 && slope.y == 1)
                {
                    PrintAnswer(3, 1, treeCountForSlope);
                }

                totalTreeCount = treeCountForSlope * totalTreeCount;
            }

            PrintAnswer(3, 2, totalTreeCount);
        }
    }
}